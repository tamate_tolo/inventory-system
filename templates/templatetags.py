#templatetags.py
from models import Instrument

from django import template

register = template.Library()

@register.inclusion_tag("create_transaction.html")
def instrument_model_list():
    brand_list = Instrument.objects.all()
    return {'instrument_list' : instrument_list}