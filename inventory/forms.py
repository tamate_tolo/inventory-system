from django import forms
from inventory.models import *
from django.utils import timezone

class MemberRoleForm(forms.ModelForm):
    class Meta:
         model = MemberRole

class MemberForm(forms.ModelForm):
    role = forms.ModelMultipleChoiceField(widget=forms.CheckboxSelectMultiple, queryset=MemberRole.objects.all().order_by('name'))
    class Meta:
        model = Member


class MusicPackForm(forms.ModelForm):
    music = forms.ModelMultipleChoiceField(widget=forms.CheckboxSelectMultiple, queryset=Music.objects.all().order_by('name'))
    class Meta:
         model = MusicPack

class InstrumentForm(forms.ModelForm):
    class Meta:
         model = Instrument


class UniformForm(forms.ModelForm):
    class Meta:
         model = Uniform

class TransactionForm(forms.ModelForm):
    instrument = forms.ModelMultipleChoiceField(widget=forms.CheckboxSelectMultiple, queryset=Instrument.objects.all().order_by('name'))
    uniform = forms.ModelMultipleChoiceField(widget=forms.CheckboxSelectMultiple, queryset=Uniform.objects.all().order_by('name'))
    class Meta:
        model = Transaction
