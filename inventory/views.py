from django.http import HttpResponse
from django.template.loader import get_template
from django.template import Context
from django.http import HttpResponseRedirect

#Needed for function views to check csrf
from django.template import RequestContext

from django.shortcuts import render_to_response
from inventory.models import *
from django.core.urlresolvers import reverse

#Security
from django.core.context_processors import csrf

#Needed for class based views
from django.views.generic import *

#Forms
from inventory.forms import *

import json

def fill_transaction_fields(request):
	from django.core import serializers
	member = request.GET['member']
	print(member)
	models = Transaction.objects.filter(member=member)
	json_models = serializers.serialize("json", models)
	print(json_models)
	return HttpResponse(json_models, mimetype="application/javascript")


def add_role(request):
    model_url = 'role-add'
    new_role = request.POST.get("rolename","")
    new_music = request.POST.get("musicname","")
    print(new_music)
    response_data= {}

    if new_music:
    	x = Music()
        x.name = new_music
        x.save()
        response_data['message'] = new_music + " was created"

    elif new_role:
        x = MemberRole()
        x.name = new_role
        x.save()
        response_data['message'] = new_role + " was created"


    else:
        response_data['message'] = 'Nothing created'

    return HttpResponse(json.dumps(response_data),content_type="application/json")


def add_member(request):
	page_name = 'Member'
	model_url = 'member-add'
	if request.method == "POST":
		mform = MemberForm(request.POST)
		if mform.is_valid():
			new_member = mform.save()
			role = mform.cleaned_data['role']
			new_member.role = role
			new_member.save()
			return HttpResponseRedirect('members')
	else:
		mform = MemberForm()
	return render_to_response('create_model.html', {'member_form': mform, 'model_url': model_url, 'page_name':page_name},context_instance=RequestContext(request))

def add_musicpack(request):
	page_name = 'Music Pack'
	model_url = 'musicpack-add'
	if request.method == "POST":
		mform = MusicPackForm(request.POST)
		if mform.is_valid():
			new_musicpack = mform.save()
			new_musicpack.save()
			return HttpResponseRedirect('musicpacks')
	else:
		mform = MusicPackForm()
	return render_to_response('create_musicpack.html', {'musicpack_form': mform, 'model_url': model_url, 'page_name':page_name},context_instance=RequestContext(request))

def add_transaction(request):
	page_name = 'Transaction'
	model_url = 'transaction-add'
	if request.method == "POST":
		tform = TransactionForm(request.POST)
		if tform.is_valid():
			new_transaction = tform.save()
			new_transaction.save()
			return HttpResponseRedirect('transactions')
	else:
		tform = TransactionForm()
		tform.member = Member.objects.filter(last_name='A')
	return render_to_response('create_transaction.html', {'transaction_form': tform, 'model_url': model_url, 'page_name':page_name},context_instance=RequestContext(request))

class HomePageView(TemplateView):

	template_name = "template.html"
	page_name = None
	
	def get_context_data(self,**kwargs):
			context = super(HomePageView, self).get_context_data(**kwargs)
			context['page_name'] = self.page_name
			return context

class ModelListView(ListView):
		
	template_name = "model_list.html"
	page_name = None
	edit_link = None

	def get_context_data(self,**kwargs):
		context = super(ModelListView, self).get_context_data(**kwargs)
		context['page_name'] = self.page_name
		context['edit_link'] = self.edit_link
		return context

class ModelCreateView(CreateView):

	model_url = None
	page_name = None
	model_list = None
	form_class = None
	template_name = 'create_model.html'

	def get_success_url(self):
		return reverse(self.model_list)

	def get_context_data(self,**kwargs):
		context = super(ModelCreateView, self).get_context_data(**kwargs)
		context['page_name'] = self.page_name
		context['model_url'] = self.model_url
		context['form_class'] =self.form_class
		return context


class ModelUpdateView(UpdateView):
	model_url = None
	page_name = None
	model_list = None
	template_name = 'update_model.html'

	def get_success_url(self):
		return reverse(self.model_list)

	def get_context_data(self,**kwargs):
		context = super(ModelUpdateView, self).get_context_data(**kwargs)
		context['page_name'] = self.page_name
		context['model_url'] = self.model_url
		context['model_list'] = self.model_list
		return context











