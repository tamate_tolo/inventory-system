from django.db import models, transaction, IntegrityError
from django.utils.translation import ugettext as _
from django.utils import timezone
import datetime

#add music model

class get_fields(object):
    def get_fields(self):
        return[(field.name, field.value_to_string(self)) for field in self.__class__._meta.fields]

class Music(models.Model,get_fields):
    name = models.CharField(max_length = 20)

    def __unicode__(self): 
        return self.name

class MusicPack(models.Model,get_fields):
    title = models.CharField(max_length = 20)
    composer = models.CharField(max_length = 20) 
    arranger = models.CharField(max_length = 20)
    music = models.ManyToManyField(Music)

    def __unicode__(self): 
        return self.title +' - '+ self.composer +' - '+ self.arranger

    #Combined Primary key
    class Meta:
        unique_together = ('title', 'composer','arranger')


class Instrument(models.Model,get_fields):
    TYPES_CHOICES = (
        ('ACADEMY', 'Academy'),
        ('SENIOR', 'Senior'),
        ('PERCUSSION', 'Percussion')
    )
    type = models.CharField(_('Type'), max_length = 20, choices = TYPES_CHOICES)
    name = models.CharField(max_length = 20)
    serial = models.CharField(max_length = 30, primary_key = True)

    def __unicode__(self): 
        return self.serial + " - " + self.name

class MemberRole(models.Model,get_fields):
    name = models.CharField(max_length = 20)
        
    def __unicode__(self): 
        return self.name

class Member(models.Model,get_fields):
    first_name = models.CharField(max_length = 20)
    last_name = models.CharField(max_length = 20)
    street_line1 = models.CharField(_('Address 1'), max_length = 100, blank = True)
    street_line2 = models.CharField(_('Address 2'), max_length = 100, blank = True)
    zipcode = models.CharField(_('ZIP code'), max_length = 5, blank = True)
    city = models.CharField(_('City'), max_length = 100, blank = True)
    number = models.CharField(max_length = 20)
    email = models.EmailField(max_length = 254)
    role = models.ManyToManyField(MemberRole)

    def __unicode__(self):
        return self.last_name + " - " + unicode(self.pk)
        
class Uniform(models.Model,get_fields):
    SIZE_CHOICES = (
        ('SMALL', 'Small'),
        ('MEDIUM', 'Medium'),
        ('LARGE', 'Large'),
        ('XLARGE', 'X Large')
    )
    size = models.CharField(_('Size'), max_length= 20, choices = SIZE_CHOICES)
    name = models.CharField(max_length = 20)
    serial = models.CharField(max_length = 30, primary_key = True)

    def __unicode__(self):
        return self.serial +" - "+ self.name

class Transaction(models.Model,get_fields):
    member = models.ForeignKey(Member)
    date = models.DateTimeField(default= timezone.now,blank=True)
    instrument = models.ManyToManyField(Instrument)
    uniform = models.ManyToManyField(Uniform)

    def __unicode__(self):
        return unicode(self.member) +" - "+ unicode(self.date)

class OtherState(models.Model):
    state = models.CharField(max_length = 20)
    def __unicode__(self): 
        return self.state

class MusicPackState(models.Model):
    TYPES_CHOICES = ( 
        ('ISSUED', 'Issued'),
        ('LIBRARY', 'Library'),
        ('LOAN', 'Loan'),
        ('ENTERTAINMENTFOLDER', 'Entertainment Folder'),
        ('OTHER', 'Other')
    )
    type = models.CharField(_('Type'), max_length = 20, choices = TYPES_CHOICES)
    member = models.ForeignKey(Member)
    music_pack = models.ForeignKey(MusicPack,null = True, blank = True)
    other_states = models.ForeignKey(OtherState, null = True, blank = True)

    def if_loan(self):
        if type in ['LIBRARY','ENTERTAINMENT','ISSUED']:
           self.member = None




