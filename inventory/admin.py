from django.contrib import admin
from inventory.models import *

admin.site.register(MusicPack)
admin.site.register(Music)
admin.site.register(MemberRole)
admin.site.register(Member)
admin.site.register(Transaction)
admin.site.register(Uniform)
admin.site.register(Instrument)


