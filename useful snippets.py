
    def clean(self):
        cleaned_data = super(Role, self).clean()
         type = cleaned_data.get("type")
        extra_roles = cleaned_data.get("extra_roles")

        #Makes it so only a role or an extra role can be added to a member in a transaction
        if type and extra_roles:
            raise forms.ValidationError("Select either only a single role from either of the fields")

        return cleaned_data