from django.conf.urls import patterns, include, url
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.contrib import admin
from woolston_bb import settings
from django.conf.urls.static import static

#Models
from inventory.models import *
#Forms
from inventory.forms import *
#Views
from inventory.views import *

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',

#List Views
    url(r'^home', HomePageView.as_view(page_name = 'Welcome'), name='home'),
    url(r'^musicpacks', ModelListView.as_view(model = MusicPack, page_name = 'Music Packs',edit_link = 'updatemusicpack/'), name='musicpack-list'),
    url(r'^instruments', ModelListView.as_view(model = Instrument,page_name = 'Instruments',edit_link = 'updateinstrument/'), name='instrument-list'),
    url(r'^members', ModelListView.as_view(model = Member,page_name = 'Members',edit_link = 'updatemember/'), name='member-list'),
    url(r'^roles', ModelListView.as_view(model = MemberRole, page_name = 'Member Roles',edit_link = 'updatememberrole/'), name='memberrole-list'),
    url(r'^uniforms', ModelListView.as_view(model = Uniform, page_name = 'Uniforms',edit_link = 'updateuniform'), name='uniform-list'),
    url(r'^transactions', ModelListView.as_view(model = Transaction, page_name = 'Transactions',edit_link = 'updatetransaction'), name='transaction-list'),

#Create Views
    url(r'^newinstrument', ModelCreateView.as_view(model = Instrument, model_url = 'instrument-new',model_list = 'instrument-list',  page_name = 'Instrument',form_class = InstrumentForm), name='instrument-new'),
    url(r'^newmemberrole', ModelCreateView.as_view(model = MemberRole, model_url = 'memberrole-new',model_list = 'memberrole-list',  page_name = 'Member Role',form_class = MemberRoleForm), name='memberrole-new'),
    
    url(r'^addrole', 'inventory.views.add_role', name = 'role-add'),
    url(r'^newmember', 'inventory.views.add_member', name = 'member-new'),

    url(r'^newuniform', ModelCreateView.as_view(model = Uniform, model_url = 'uniform-new',model_list = 'uniform-list',  page_name = 'Uniform',form_class = UniformForm), name='uniform-new'),
    url(r'^newtransaction', 'inventory.views.add_transaction', name = 'transaction-new'),
    url(r'^newmusicpack', 'inventory.views.add_musicpack', name = 'musicpack-new'), 
    url(r'^filltransactionfields', 'inventory.views.fill_transaction_fields'),

#Update Views
    url(r'^updatememberrole/(?P<pk>[\w-]+)$', ModelUpdateView.as_view(model = MemberRole, model_url = 'memberrole-new',model_list = 'memberrole-list',page_name = 'Member Role'), name='memberrole-update'),
    url(r'^updatemusicpack/(?P<pk>[\w-]+)$', ModelUpdateView.as_view(model = MusicPack, model_url = 'musicpack-new',model_list = 'musicpack-list',page_name = 'Music Pack'), name='musicpack-update'),
    url(r'^updatemember/(?P<pk>[\w-]+)$', ModelUpdateView.as_view(model = Member, model_url = 'member-new',model_list = 'member-list',page_name = 'Member'), name='member-update'),
    url(r'^updateinstrument/(?P<pk>[\w-]+)$', ModelUpdateView.as_view(model = Instrument, model_url = 'instrument-new',model_list = 'instrument-list', page_name = 'Instrument'), name='instrument-update'),
    url(r'^updateuniform/(?P<pk>[\w-]+)$', ModelUpdateView.as_view(model = Uniform, model_url = 'uniform-new',model_list = 'uniform-list', page_name = 'Uniform'), name='uniform-update'),
    url(r'^updatetransaction/(?P<pk>[\w-]+)$', ModelUpdateView.as_view(model = Transaction, model_url = 'transacction-new',model_list = 'transaction-list', page_name = 'Transaction'), name='transaction-update'),


    # Uncomment the admin/doc line below to enable admin documentation:
    url(r'^admin/doc/', include('django.contrib.admindocs.urls')),
    # Uncomment the next line to enable the admin:
    url(r'^admin/', include(admin.site.urls)),
)
urlpatterns += staticfiles_urlpatterns()